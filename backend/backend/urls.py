

from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.views.generic import RedirectView

from backend.settings import base as app_settings
from app import urls as app_urls


admin.site.site_header = 'Chapp Solutions App'
admin.autodiscover()

urlpatterns = [

    # Default Urls
    path('', RedirectView.as_view(url='/admin/')),

    # Admin Urls
    path('admin/', admin.site.urls),

    # App Urls
    path('app/', include((app_urls, 'app'), namespace='app')),
]

if app_settings.FILES == 'LOCAL':
    urlpatterns += static(app_settings.STATIC_URL, document_root=app_settings.STATIC_ROOT)
    urlpatterns += static(app_settings.MEDIA_URL, document_root=app_settings.MEDIA_ROOT)
