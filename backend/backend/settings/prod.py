
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os


# ********** Environment Vars **********

SECRET_KEY = os.environ.get('SECRET_KEY')
#print('secret key: ' + str(SECRET_KEY))

DEBUG = os.environ.get('DEBUG')
#print('debug: ' + str(DEBUG))

APP_DOMAIN = os.environ.get('DOMAIN')
#print('domain: ' + APP_DOMAIN)

APP_FRONTEND_DOMAIN = os.environ.get('FRONTEND_DOMAIN')




#
