
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os


# ********** Database **********

DB_NAME = os.environ.get('DB_PROD_NAME')
DB_USER = os.environ.get('DB_PROD_USER')
DB_PASSWORD = os.environ.get('DB_PROD_PASSWORD')
DB_HOST = os.environ.get('DB_PROD_HOST')
DB_PORT = os.environ.get('DB_PROD_PORT')




#
