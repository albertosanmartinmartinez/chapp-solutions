
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import environ

env = environ.Env()
environ.Env.read_env()


# ********** Environment Vars **********

SECRET_KEY = env.str('SECRET_KEY')
#print('secret key: ' + str(SECRET_KEY))

DEBUG = env.str('DEBUG')
#print('debug: ' + str(DEBUG))

APP_DOMAIN = env.str('DOMAIN')
#print('domain: ' + APP_DOMAIN)

APP_FRONTEND_DOMAIN = env.str('FRONTEND_DOMAIN')
#print('domain: ' + APP_FRONTEND_DOMAIN)

ERRORS = env.str('ERRORS')
#print('errors: ' + ERRORS)

DJANGO_SUPERUSER_EMAIL = env.str('DJANGO_SUPERUSER_EMAIL')
#print('errors: ' + DJANGO_SUPERUSER_EMAIL)

DJANGO_SUPERUSER_PASSWORD = env.str('DJANGO_SUPERUSER_PASSWORD')
#print('errors: ' + DJANGO_SUPERUSER_PASSWORD)



#
