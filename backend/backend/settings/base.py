
#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import environ
import datetime

from pathlib import Path


BASE_DIR = Path(__file__).resolve().parent.parent.parent
#print(BASE_DIR)

APP_DIR = os.path.join(BASE_DIR, "app")
#print(APP_DIR)

PROJECT_DIR = os.path.join(BASE_DIR, "backend")
#print(PROJECT_DIR)

STATIC_DIR  = os.path.join(BASE_DIR, "static")
#print(STATIC_DIR)

MEDIA_DIR  = os.path.join(BASE_DIR, "user_data")
#print(MEDIA_DIR)

TEMPLATES_DIR  = os.path.join(BASE_DIR, "templates")
#print(TEMPLATES_DIR)


env = environ.Env()
environ.Env.read_env()

# ********** Environment Vars **********

STATUS = 'DEV'
#STATUS = 'PROD'
#print('status: ' + STATUS)

from backend.settings.dev import *

#print('debug : ' + DEBUG)

DB_DEPLOY = 'LOCAL'
#DB_DEPLOY = 'CLOUD'
#print('db : ' + DB_DEPLOY)

from backend.settings.dev_db import *

EMAILS = True
#EMAILS = False
#print('emails: ' + str(EMAILS))

if EMAILS:
    from backend.settings.dev_mail import *

else:
    from backend.settings.prod_mail import *


# ********** Django Config **********

ALLOWED_HOSTS = [
    '*'
]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    #'corsheaders',
    'rest_framework',
    'django_filters',

    'app.apps.ChappSolutionsAppConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTH_USER_MODEL = 'app.User'

ROOT_URLCONF = 'backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            TEMPLATES_DIR,
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# ********** Server **********

WSGI_APPLICATION = 'backend.wsgi.application'


# ********** Backends **********

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]


# ********** Database **********

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}


# ********** Password Validators **********

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# ********** Internationalization **********

LANGUAGE_CODE = 'es-ES'

TIME_ZONE = 'Europe/Madrid'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# ********** Files **********

FILE_OVERWRITE = True
#FILE_OVERWRITE = False

FILES = 'LOCAL'
#FILES = 'CLOUD'
#print('files: ' + FILES)

from backend.settings.dev_files import *


# ********** Static Files **********

STATICFILES_FINDERS = [
   "django.contrib.staticfiles.finders.FileSystemFinder",
   "django.contrib.staticfiles.finders.AppDirectoriesFinder"
]

STATICFILES_DIRS = [
    STATIC_DIR
]

STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")


# ********** Media Files **********

MEDIAFILES_DIRS = [
    MEDIA_DIR
]

MEDIA_ROOT = os.path.join(BASE_DIR, "media")


# ********** Django Rest Framework **********

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20
}


# ********** Cors Headers **********

CORS_ORIGIN_ALLOW_ALL = True

CORS_ORIGIN_WHITELIST = [
    '*'
]






#
