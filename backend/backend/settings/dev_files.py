
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import environ

from django.core.files.storage import default_storage
#from django.contrib.staticfiles.storage import StaticFilesStorage
#from django.core.files.storage import FileSystemStorage

from backend.settings import base as app_settings

env = environ.Env()
environ.Env.read_env()


# ********** Files **********

DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'


# ********** Amazon Web Services **********

AWS_ACCESS_KEY_ID = env.str('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env.str('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = env.str('AWS_BUCKET_NAME')
AWS_S3_CUSTOM_DOMAIN = '%s.s3.eu-west-1.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_DEFAULT_ACL = 'public-read'
AWS_S3_FILE_OVERWRITE = app_settings.FILE_OVERWRITE


# ********** Static Files **********

STATIC_LOCATION = 'static'
STATIC_URL = '/static/'
#STATICFILES_STORAGE = 'django.core.files.storage.FileSystemStorage'
#STATICFILES_STORAGE = 'app.storages.StaticStorage'

# ********** Media Files **********

MEDIA_LOCATION = 'user_data'
#MEDIAFILES_STORAGE = 'django.core.files.storage.FileSystemStorage'
MEDIAFILES_STORAGE = 'app.storages.CustomStorage'
MEDIA_URL = '/media/'




#
