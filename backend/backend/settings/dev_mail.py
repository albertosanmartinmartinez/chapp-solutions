
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import environ

env = environ.Env()
environ.Env.read_env()


# ********** Mail **********

EMAIL_USE_TLS = True
#print(EMAIL_USE_TLS)
EMAIL_USE_SSL = False
#print(EMAIL_USE_SSL)
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = env.str('EMAIL_HOST')
#print(EMAIL_HOST)
EMAIL_HOST_USER = env.str('EMAIL_USER')
#print(EMAIL_HOST_USER)
EMAIL_HOST_PASSWORD = env.str('EMAIL_PASSWORD')
#print(EMAIL_HOST_PASSWORD)
EMAIL_PORT = 587
#print(EMAIL_PORT)
#EMAIL_PORT = 465




#
