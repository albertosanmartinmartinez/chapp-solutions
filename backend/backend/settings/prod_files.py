
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from backend.settings import base as app_settings


# ********** Files **********

DEFAULT_FILE_STORAGE = 'app.storages.MediaStorage'


# ********** Amazon Web Services **********

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
#AWS_STORAGE_REGION_NAME = os.environ.get('AWS_STORAGE_REGION_NAME')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_BUCKET_NAME')
AWS_S3_CUSTOM_DOMAIN = '%s.s3.eu-west-1.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
#AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_DEFAULT_ACL = 'public-read'
AWS_S3_FILE_OVERWRITE = app_settings.FILE_OVERWRITE


# ********** Static Files **********

STATIC_LOCATION = 'static'
STATICFILES_STORAGE = 'app.storages.StaticStorage'
STATIC_URL='https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, STATIC_LOCATION)


# ********** Media Files **********

MEDIA_LOCATION = 'media'
MEDIAFILES_STORAGE = 'app.storages.MediaStorage'
MEDIA_URL='https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, MEDIA_LOCATION)



#
