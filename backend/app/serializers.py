
from rest_framework import serializers, fields
from django.contrib.auth import get_user_model

from app import models as app_models


# Create you serializers here
class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = '__all__'


class RoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_models.Room
        fields = '__all__'


class ReservationSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_models.Reservation
        fields = '__all__'
