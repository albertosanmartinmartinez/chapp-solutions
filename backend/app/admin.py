
from django.contrib import admin
from django.contrib.auth import get_user_model

from app import models as app_models

# Register your admin models here.
class UserAdmin(admin.ModelAdmin):

    list_display = ('id', 'first_name', 'last_name', 'email', 'phone')
    #search_fields = ()
    #list_filter = ()
    #list_editable = ()

admin.site.register(get_user_model(), UserAdmin)


class RoomAdmin(admin.ModelAdmin):

    list_display = ('id', 'type', 'price', 'max_availability')
    #search_fields = ()
    list_filter = ('type',)
    list_editable = ('type',)

admin.site.register(app_models.Room, RoomAdmin)


class ReservationAdmin(admin.ModelAdmin):

    list_display = ('id', 'checkin_date', 'checkout_date', 'days', 'guest_number', 'total_price', 'code', 'room', 'user')
    search_fields = ('code',)
    list_filter = ('room', 'user')
    list_editable = ('checkin_date', 'checkout_date', 'guest_number')

admin.site.register(app_models.Reservation, ReservationAdmin)




#
