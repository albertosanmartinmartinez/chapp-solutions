
import json

from django.shortcuts import render
from django.http import JsonResponse

from django_filters import rest_framework as filters

from rest_framework import generics, status, permissions
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.renderers import JSONRenderer

from backend.settings import base as app_settings

from app import paginations as app_paginations
from app import serializers as app_serializers
from app import filters as app_filters
from app import models as app_models


# Create your views here.
class GenericListApiView(generics.ListAPIView):

    model = None
    pagination_class = app_paginations.CustomPageNumberPagination
    permission_classes = (permissions.AllowAny,)
    filter_backends = (filters.DjangoFilterBackend,)

    def dispatch(self, request, *args, **kwargs):
        """
        """

        print('GenericListApiView')

        # serializer
        serializer_class_name = self.model.__name__ + 'Serializer'
        self.serializer_class = getattr(__import__('app.serializers', fromlist=[serializer_class_name]), serializer_class_name)

        # filters
        filter_class_name = self.model.__name__ + 'ApiFilter'
        self.filter_class = getattr(__import__('app.filters', fromlist=[filter_class_name]), filter_class_name)

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):

        return self.model.objects.all()


class GenericDetailApiView(generics.RetrieveAPIView):

    model = None
    permission_classes = (permissions.AllowAny,)

    def dispatch(self, request, *args, **kwargs):

        # serializer
        serializer_class_name = self.model.__name__ + 'Serializer'
        self.serializer_class = getattr(__import__('app.serializers', fromlist=[serializer_class_name]), serializer_class_name)

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):

        return self.model.objects.all()





#
