from django.apps import AppConfig


class ChappSolutionsAppConfig(AppConfig):

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'
    verbose_name = 'Chapp Solutions App'
    label = 'app'
