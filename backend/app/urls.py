
from django.urls import path

from app import models as app_models
from app import views as app_views


urlpatterns = [

    # User
    path('user/', app_views.GenericListApiView.as_view(model=app_models.User), name='user_list'),
    path('user/<pk>/', app_views.GenericDetailApiView.as_view(model=app_models.User), name='user_detail'),
    #path('user/<id>/friends/', app_views.friends, name='user_friends'),

    # Room
    path('room/', app_views.GenericListApiView.as_view(model=app_models.Room), name='room_list'),
    path('room/<pk>/', app_views.GenericDetailApiView.as_view(model=app_models.Room), name='room_detail'),

    # Reservation
    path('reservation/', app_views.GenericListApiView.as_view(model=app_models.Reservation), name='reservation_list'),
    path('reservation/<pk>/', app_views.GenericDetailApiView.as_view(model=app_models.Reservation), name='reservation_detail'),
]
