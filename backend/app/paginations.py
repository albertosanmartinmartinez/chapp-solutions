
#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from collections import OrderedDict

from rest_framework import pagination, response


# Register your paginations here.
class CustomPageNumberPagination(pagination.PageNumberPagination):

    page_size = 999999
    page_size_query_param = 'page_size'
    max_page_size = 999999
    page_query_param = 'page_number'

    def get_paginated_response(self, data):

        return response.Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))




#
