
import uuid
import datetime
import decimal

from django.db import models
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.core.validators import MinLengthValidator, MaxLengthValidator

# Create your models here.
ROOM_TYPE = (("1", "Individual"), ("2", "Doble"), ("3", "Triple"), ("4", "Cuadruple"))
ROOM_PRICE = {
    "Individual": 20.00,
    "Doble": 30.00,
    "Triple": 40.00,
    "Cuadruple": 50.00
}
ROOM_NUMBER = {
    "Individual": 10,
    "Doble": 5,
    "Triple": 4,
    "Cuadruple": 6
}

class CustomUserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):

        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, password, **extra_fields):

        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)

        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):

        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must have is_staff=True.'
            )
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must have is_superuser=True.'
            )
        if extra_fields.get('is_active') is not True:
            raise ValueError(
                'Superuser must have is_active=True.'
            )

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):

    objects = CustomUserManager()

    username = None
    email = models.EmailField(verbose_name='Email', max_length=250, unique=True)
    password = models.CharField(verbose_name='Password', max_length=250, blank=True)
    phone = models.CharField(verbose_name='Phone', max_length=12, blank=True, null=True, validators=[MinLengthValidator(9, 'The phone must contain at least 9 characters'), MaxLengthValidator(12, 'The phone must contain a maximum of 12 characters.')])

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        ordering = ['email']

    def __str__(self):

        return str(self.email)

    def save(self, *args, **kwargs):

        if self.password is None:
            self.password = make_password('Abcd.1234')

        if self.email is None:
            self.username = self.email

        super(User, self).save(*args, **kwargs)


class Room(models.Model):

    type = models.CharField(verbose_name="Type", max_length=10, choices=ROOM_TYPE)
    price = models.DecimalField(verbose_name="Price", max_digits=6, decimal_places=2, blank=True)
    max_availability = models.PositiveIntegerField(verbose_name="Max availability", blank=True)

    class Meta:
        verbose_name = 'Room'
        verbose_name_plural = 'Rooms'
        ordering = ['id']

    def __str__(self):

        return self.get_type_display()

    def save(self, *args, **kwargs):

        #print('Room save method')

        # get price and max_availability from room type
        if self.type is not None:
            self.price = ROOM_PRICE.get(self.get_type_display(), None)
            self.max_availability = ROOM_NUMBER.get(self.get_type_display(), None)

        super(Room, self).save(*args, **kwargs)


class Reservation(models.Model):

    checkin_date = models.DateField(verbose_name="Check-in date")
    checkout_date = models.DateField(verbose_name="Check-out date")
    guest_number = models.PositiveIntegerField(verbose_name="Guests")
    total_price = models.DecimalField(verbose_name="Price", max_digits=7, decimal_places=2, blank=True)
    code = models.CharField(verbose_name='Locator code', max_length=50, blank=True)
    days = models.PositiveIntegerField(verbose_name="Days", blank=True)
    ###
    room = models.ForeignKey('Room', verbose_name="Room", related_name='room_reservations', on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), verbose_name="User", related_name='user_reservations', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Reservation'
        verbose_name_plural = 'Reservations'
        ordering = ['id']

    def __str__(self):

        return str(self.code)

    def save(self, *args, **kwargs):

        #print('Reservation save method')

        if self.code is None or self.code == '':
            self.code = str(uuid.uuid4())

        # calculate day from checkin and checkout dates
        delta = self.checkout_date - self.checkin_date
        self.days = delta.days

        # calculate romm total price from days and price
        self.total_price = self.room.price * decimal.Decimal(float(self.days))

        super(Reservation, self).save(*args, **kwargs)





#
