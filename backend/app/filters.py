
from django_filters import rest_framework as filters
from django.contrib.auth import get_user_model

from app import models as app_models


# Create your filters here.
class UserApiFilter(filters.FilterSet):

    email = filters.CharFilter(field_name='email', lookup_expr='icontains')

    class Meta:
        model = get_user_model()
        fields = ['email']


class RoomApiFilter(filters.FilterSet):

    type = filters.CharFilter(field_name='type', lookup_expr='icontains')
    #price
    #max_availability
    ###
    #availability

    class Meta:
        model = app_models.Room
        fields = ['type']


class ReservationApiFilter(filters.FilterSet):

    #checkin_date
    #checkout_date
    #guest_number
    #total_price
    #code
    #days
    #room
    user = filters.NumberFilter(field_name='user__id', lookup_expr='exact')

    class Meta:
        model = app_models.Reservation
        fields = ['user']
