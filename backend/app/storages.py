
#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import datetime
from django.core.files.storage import FileSystemStorage, default_storage
#from django.utils import timezone

from storages.backends.s3boto3 import S3Boto3Storage
#from storages.backends.s3boto3 import S3BotoStorage

from project.settings import base

import os


#class OverwriteStorage(FileSystemStorage):
class CustomStorage(FileSystemStorage):
    """
    """

    def get_available_name(self, name, *args, **kwargs):
        """
        """

        #print("get available name")
        #print(self)
        #print(type(self))
        #print(name)

        if self.exists(name):

            if base.FILE_OVERWRITE == False:

                alternative_name = name.split('.')
                #name = alternative_name[0] + str(timezone.localtime()).split(' ')[0] + '.' + alternative_name[1]
                name = alternative_name[0] + datetime.datetime.now().strftime('%m-%d-%Y-%H-%M-%S') + alternative_name[1]

            else:
                os.remove(os.path.join(base.MEDIA_ROOT, name))

        return name


class StaticStorage(S3Boto3Storage):
    location = base.STATIC_LOCATION
    default_acl = 'public-read'
    file_overwrite = base.FILE_OVERWRITE


class MediaStorage(S3Boto3Storage):
    location = base.MEDIA_LOCATION
    #default_acl = 'public-read'
    default_acl = 'public-read'
    file_overwrite = base.FILE_OVERWRITE




#
